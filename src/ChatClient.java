import java.io.IOException;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;

public class ChatClient extends  AbstractClient {

	public ChatClient(String host, int port) {
		super(host, port);
	}

	protected void handleMessageFromServer(Object message) {
		System.out.println("> "+message);
	}

	public static void main(String[] args) {
		ChatClient client = new ChatClient("158.108.237.123",5555);
		try{
			client.openConnection();
			System.out.println("Connected to server "+client.getInetAddress().getHostAddress());
			Scanner input = new Scanner(System.in);
			String message = "";
			while(client.isConnected()){
				message = input.nextLine();
				if(message.equals("quit")){
					client.closeConnection();
				}
				else{
					client.sendToServer(message);
				}
			}
			System.out.println("Disconnect");
		}
		catch(IOException e){
			System.out.println("Can't connect to server");
		}

	} 



}
