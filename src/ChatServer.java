import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

public class ChatServer extends AbstractServer {

	private final int LOGIN = 2;
	private final int LOGOUT = 0;
	private List<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();
	private String whispername = "";

	/** create a new chat server */
	public ChatServer(int port) {
		super(port);
	}

	protected void clientConnected(ConnectionToClient client){
		clients.add(client);
		client.setInfo("state", LOGOUT);
		sendToClient(client,"Plase login");
	}
	protected synchronized void clientDisconnected(ConnectionToClient client){
		sendToAllClients(client.getInfo("username")+" Left chat");
		clients.remove(client);
	}
	
	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		if(!(msg instanceof String)){
			sendToClient(client,"Unrecognized Message");
		}
		int state = (Integer)(client.getInfo("state"));
		String message = (String)msg;
		switch(state){
		case LOGOUT:
			if(message.matches("Login \\w+")){
				String username = message.substring(6).trim();
				client.setInfo("username", username);
				client.setInfo("state", LOGIN);
				sendToAllClients(client.getInfo("username")+" has joined.");
			}
			else{
				sendToClient(client,"Plase login");
				return ;
			}
			break;
		case LOGIN:
			if(message.equalsIgnoreCase("logout")){
				client.setInfo("state", LOGOUT);
				sendToClient(client,"Goodbye");
			}
			
			else if(message.split(" ")[0].equalsIgnoreCase("to:")){
				for(ConnectionToClient c : clients ){
					if(message.split(" ")[1].equalsIgnoreCase(c.getInfo("username")+"")){
						whispername = c.getInfo("username")+"";
					}
				}
			}
			
			else if(whispername.length()>0){
				for(ConnectionToClient c : clients ){
					if(whispername.equalsIgnoreCase(c.getInfo("username")+"")){
						sendToClient(c,client.getInfo("username")+": "+message);
						whispername="";
					}
				}
			}
			
			else{
				String username = (String)client.getInfo("username");
				super.sendToAllClients(username+": "+message);
			}
			break;
		}
	}


	private void sendToClient(ConnectionToClient client, String string) {
		try{
			client.sendToClient(string);
		}catch(IOException e){

		}

	}


	private static final int PORT = 5555;

	public static void main(String[] args) {
		ChatServer server = new ChatServer(PORT);
		try {
			server.listen();
			System.out.printf("Listening on port %d\n",PORT);
		} catch (IOException e) {
			System.out.println("Couldn't start server:");
			System.out.println(e);
		}
	}
}
